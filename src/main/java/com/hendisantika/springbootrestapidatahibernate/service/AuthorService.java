package com.hendisantika.springbootrestapidatahibernate.service;

import com.hendisantika.springbootrestapidatahibernate.entity.Author;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-data-hibernate
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-19
 * Time: 07:26
 * To change this template use File | Settings | File Templates.
 */
public interface AuthorService {
    boolean isExist(Author author);

    Author save(Author author);

    Author findById(int id);

    List<Author> findAll();

    Author update(Author author);

    void delete(int id);

    void deleteAll();
}
