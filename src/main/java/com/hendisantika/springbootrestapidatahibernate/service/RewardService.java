package com.hendisantika.springbootrestapidatahibernate.service;

import com.hendisantika.springbootrestapidatahibernate.entity.Reward;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-data-hibernate
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-19
 * Time: 07:30
 * To change this template use File | Settings | File Templates.
 */
public interface RewardService {

    boolean isExist(Reward reward);

    Reward save(Reward reward);

    Reward findById(int id);

    Reward update(Reward reward);
}