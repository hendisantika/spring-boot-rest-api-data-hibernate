package com.hendisantika.springbootrestapidatahibernate.repository;

import com.hendisantika.springbootrestapidatahibernate.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-data-hibernate
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-19
 * Time: 07:23
 * To change this template use File | Settings | File Templates.
 */
@RepositoryRestResource
public interface AuthorRepository extends JpaRepository<Author, Integer> {
}