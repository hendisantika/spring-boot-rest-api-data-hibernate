package com.hendisantika.springbootrestapidatahibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestApiDataHibernateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestApiDataHibernateApplication.class, args);
    }

}

